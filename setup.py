import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="profiling",
    version="0.0.1",
    author="Jeroen van Heiningen",
    author_email="jvanheiningen@cloudsuite.com",
    description="A small profiling package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    # url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "Operating System :: OS Independent",
    ],
    python_requires='>=2.7',
    install_requires=[
          'guppy',
      ],
)