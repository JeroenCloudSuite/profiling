import pstats
import cProfile
import datetime
import StringIO
from guppy import hpy


def profile(file=None):
    """ decorator to profile functions execution """
    def wrap(function):
        def wrapper(*args, **kwargs):
            pr = cProfile.Profile()
            pr.enable()
            return_value = function(*args, **kwargs)
            pr.disable()
            s = StringIO.StringIO()
            ps = pstats.Stats(pr, stream=s).sort_stats('cumulative')
            ps.print_stats()
            if file:
                with open(file, "a") as f:
                    now = datetime.datetime.now()
                    f.write("function: {0} ({1})\n".format(
                        function.__name__, now.isoformat()))
                    f.write(s.getvalue())
            else:
                print(s.getvalue())
            return return_value
        return wrapper
    return wrap


def profile_mem(file=None):
    """ decorator to profile functions memory usage """
    def wrap(function):
        def wrapper(*args, **kwargs):
            hp1 = str(hpy().heap())
            now = datetime.datetime.now()
            return_value = function(*args, **kwargs)
            hp2 = str(hpy().heap())
            if file:
                with open(file, "a") as f:
                    f.write("\nBEFORE RUNNING {0} ({1})\n".format(
                        function.__name__, now.isoformat()))
                    f.write(hp1)
                    f.write("\nAFTER\n")
                    f.write(hp2)
            else:
                print("BEFORE RUNNING {0} ({1})".format(
                        function.__name__, now.isoformat()))
                print(hp1)
                print("AFTER")
                print(hp2)
            return return_value
        return wrapper
    return wrap
