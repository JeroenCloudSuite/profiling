from profiling import profile, profile_mem
from guppy import hpy


@profile('profile.log')
def test(name):
    result = []
    for i in range(10000):
        result.append(name)
    return result


@profile_mem('mem.log')
# @profile()
def do_it():
    result = '20'
    for i in range(20000):
        result += 'a' * i
    return result


b = test('asd')
a = do_it()


print(hpy().heap())
